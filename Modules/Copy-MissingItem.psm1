function Copy-MissingItem
{
    $source = $args[0]
    $destination = $args[1]
    $sourcelist = @(Get-ChildItem $source | Where {$_.psIsContainer -eq $false})
    $destinationlist = @(Get-ChildItem $destination)

    $ExcludeItems = @(Compare-Object $sourcelist $destinationlist -excludeDifferent -includeEqual)

    Copy-Item "$Source\*" -Destination "$Destination" -Exclude $ExcludeItems -Recurse
 }