<#
From PowerShell:
iex ((new-object net.webclient).DownloadString('https://bitbucket.org/andrew-bond/dotfiles-windows/raw/master/setup/install.ps1'))

Doesn't work because git not installed by default:
git clone https://bitbucket.org/andrew-bond/dotfiles-windows.git; cd dotfiles-windows; . .\bootstrap.ps1

#>
$profileDir = Split-Path -parent $profile
$componentDir = Join-Path $profileDir "components"
if (![System.IO.Directory]::Exists($profileDir)) {[System.IO.Directory]::CreateDirectory($profileDir)}
if (![System.IO.Directory]::Exists($componentDir)) {[System.IO.Directory]::CreateDirectory($componentDir)}
Copy-Item -Path ./*.ps1 -Destination $profileDir -Exclude "bootstrap.ps1"
Copy-Item -Path ./components/** -Destination $componentDir -Include **
Copy-Item -Path ./homeFiles/** -Destination $home -Include **
Remove-Variable componentDir
Remove-Variable profileDir