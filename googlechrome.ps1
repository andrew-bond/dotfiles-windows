﻿. (Join-Path (split-path -parent $MyInvocation.MyCommand.Definition) "StrictMode.ps1")

# Chrome should be installed by now so configure it
$registryPath = "HKCU:\SOFTWARE\Policies\Google\Chrome"

IF(!(Test-Path $registryPath))
  {
    New-Item -Path $registryPath -Force | Out-Null
}

Set-ItemProperty -Path $registryPath -Name RestoreOnStartup -Value 1
#Set-ItemProperty -Path $registryPath -Name DnsPrefetchingEnabled -Value 1
Set-ItemProperty -Path $registryPath -Name ForceGoogleSafeSearch -Value 0
Set-ItemProperty -Path $registryPath -Name SpellCheckServiceEnabled -Value 1

$registryPath = "HKLM:\SOFTWARE\Policies\Google\Chrome"

IF(!(Test-Path $registryPath))
{
    New-Item -Path $registryPath -Force | Out-Null
}

Set-ItemProperty -Path $registryPath -Name DnsPrefetchingEnabled -Value 1

Remove-Variable registryPath

$chromePath = "${Env:ProgramFiles(x86)}\Google\Chrome\Application\" 
$chromeApp = "chrome.exe"
$chromeCommandArgs = "--make-default-browser"
& "$chromePath$chromeApp" $chromeCommandArgs