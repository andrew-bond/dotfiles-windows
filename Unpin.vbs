'Credit goes to Eric G. for writing this script.
'More info at http://frontslash.wordpress.com/2010/03/01/removing-internet-explorer-and-windows-media-player-from-taskbar/#comment-178

Option Explicit

' %appdata%\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar

Const CSIDL_COMMON_PROGRAMS = &H17
Const CSIDL_PROGRAMS = &H2
Const CSIDL_STARTMENU = &HB
Const CSIDL_APPDATA = &H1A

Dim objShell, objFSO
Dim objCurrentUserAppDataFolder
Dim strCurrentUserAppDataFolderPath
Dim strTaskBarPath
Dim objFolder
Dim objFolderItem
Dim colVerbs
Dim objVerb
Dim i

Dim shortcutsToUnPin(2)
shortcutsToUnPin(0) = "Windows Media Player.lnk"
shortcutsToUnPin(1) = "Windows Expplorer.lnk"
shortcutsToUnPin(2) = "Google Chrome.lnk"



Set objShell = CreateObject("Shell.Application")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objCurrentUserAppDataFolder = objShell.NameSpace (CSIDL_APPDATA)
strCurrentUserAppDataFolderPath = objCurrentUserAppDataFolder.Self.Path
strTaskBarPath = strCurrentUserAppDataFolderPath & "\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar\"

For i = 0 to UBound(shortcutsToUnPin)
	If objFSO.FileExists(strTaskBarPath & shortcutsToUnPin(i)) Then
		Set objFolder = objShell.Namespace(strTaskBarPath)
		Set objFolderItem = objFolder.ParseName(shortcutsToUnPin(i))
		Set colVerbs = objFolderItem.Verbs
		For Each objVerb in colVerbs
			If Replace(objVerb.name, "&", "") = "Unpin from Taskbar" Then objVerb.DoIt
		Next
	End If
Next