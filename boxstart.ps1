# Normally I'd enable StrictMode but Chocolatey / Boxstarter doesn't seem to play well with StrictMode

# Todo - Reimplement as best in class, update readme.md

# WARNING - Will reboot repeatedly without warning or confirmation

# Launch this from IE and the Boxstarter launcher should install and run. Note that this will not work on Chrome or Firefox unless you have a "Click-Once" extension.

$installString = @"
http://boxstarter.org/package/launchy,lastpass,dropbox
http://boxstarter.org/package/git,google-chrome-x64,peazip,notepadplusplus,winmerge,firefox,clink,sourcetree,autohotkey,synergy,Boxstarter.Chocolatey,autoit,treesizefree
"@

# dotnet4.5,
# dotnet4.5 is needed for sourcetree as of 2015-11
# ,powershell
# https://chocolatey.org/packages/PowerShell
# $PSVersionTable.PSVersion

<# 
http://boxstarter.org/package/nr/url?https://bitbucket.org/andrew-bond/dotfiles-windows-andrewbond/raw/master/boxstart.ps1 %USERPROFILE%\projects\
git clone https://andrew-bond@bitbucket.org/andrew-bond/dotfiles-windows-andrewbond.git 
#>


Import-Module Boxstarter.Chocolatey
$Boxstarter.RebootOk=$false # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true

Update-ExecutionPolicy Unrestricted

Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name TaskbarGlomLevel -Value 1
Set-TaskbarOptions -Size Small

# Ideally I'd only use a regex but the regex I'd expect to work isn't working in PowerShell
$installString = $installString.SubString($installString.lastindexof("/") + 1)

$packageNames = ([regex]::matches($installString, "([^,`"]+)+"))

foreach($packageName in $packageNames)
{
    cinst -y $packageName
}

Remove-Variable installString
Remove-Variable packageName
Remove-Variable packageNames

cinst -y iTunes -Version 12.3
# cinst -y flashplayerplugin
# cinst -y IrfanView
# cinst -y Quicktime
# cinst -y skype

Enable-MicrosoftUpdate
Install-WindowsUpdate -acceptEula -SuppressReboots

. ./windows2.ps1