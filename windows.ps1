# If you've never executed a PS1 file before on your system, run the following command from PowerShell
Set-ExecutionPolicy -Scope CurrentUser Unrestricted -force

Set-Location (split-path -parent $MyInvocation.MyCommand.Definition)
. ./StrictMode.ps1

#region Identity

# Get the ID and security principal of the current user account
$myIdentity=[System.Security.Principal.WindowsIdentity]::GetCurrent()
$myPrincipal=new-object System.Security.Principal.WindowsPrincipal($myIdentity)

# Check to see if we are currently running "as Administrator"
if (!$myPrincipal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)) {
   $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";
   $newProcess.Arguments = $myInvocation.MyCommand.Definition;
   $newProcess.Verb = "runas";
   [System.Diagnostics.Process]::Start($newProcess);

   exit
}

## Set DisplayName for my account
## Useful for setting up Account information if you are not using a Microsoft Account
#$userFullName   = "Jay Harris"
#$user = Get-WmiObject Win32_UserAccount | Where {$_.Caption -eq $myIdentity.Name}
#$user.FullName = $userFullName
#$user.Put() | Out-Null
#Remove-Variable userFullName
#Remove-Variable user

Remove-Variable myPrincipal
Remove-Variable myIdentity

#endregion

. ./Windows2.ps1

# Check if Boxstarter.Chocolatey is available
if (!(Get-Module -ListAvailable -Name BoxStarter.Chocolatey)) {
    # If not, install it
    start "http://boxstarter.org/package/nr/Boxstarter.Chocolatey"

    Function Pause ($Message = "Opening Boxstarter installer, then press any key to continue . . . ") {
        if ((Test-Path variable:psISE) -and $psISE) {
            $Shell = New-Object -ComObject "WScript.Shell"
            $Button = $Shell.Popup("Click OK to continue.", 0, "Script Paused", 0)
        }
        else {     
            Write-Host -NoNewline $Message
            [void][System.Console]::ReadKey($true)
            Write-Host
        }
    }

    Pause
}

# Chocolatey / Boxstarter doesn't seem to play well with StrictMode
Set-StrictMode -Off

Import-Module Boxstarter.Chocolatey
Invoke-ChocolateyBoxstarter ./boxstart.ps1 -DisableReboots