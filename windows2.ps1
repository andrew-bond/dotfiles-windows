﻿Set-Location (split-path -parent $MyInvocation.MyCommand.Definition)
. .\StrictMode.ps1

# Get the ID and security principal of the current user account
$myIdentity=[System.Security.Principal.WindowsIdentity]::GetCurrent()
$myPrincipal=new-object System.Security.Principal.WindowsPrincipal($myIdentity)

# Check to see if we are currently running "as Administrator"
if (!$myPrincipal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)) {
   $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";
   $newProcess.Arguments = $myInvocation.MyCommand.Definition;
   $newProcess.Verb = "runas";
   [System.Diagnostics.Process]::Start($newProcess);

   exit
}

# FType Microsoft.PowerShellScript.1=$PSHOME\powershell.exe -NoExit . "'%1'" %*; Exit

# REG.EXE add HKCU\Console /v QuickEdit /t REG_DWORD /d 1 /f

tzutil /s "Eastern Standard Time"

function set-machineName
{
    # $machineName = Read-Host -Prompt "Computer name: $env:computername. Enter new computer name or enter to skip"

    # Set Computer Name
    if ($machineName) { (Get-WmiObject Win32_ComputerSystem).Rename($machineName) }

    Remove-Variable machineName
}

# Sound: Disable Startup Sound
Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" "DisableStartupSound" 1
Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\BootAnimation" "DisableStartupSound" 1
Clear-ItemProperty "HKCU:\AppEvents\Schemes\Apps\Explorer\Navigating\.Current" "(Default)"
Clear-ItemProperty "HKCU:\AppEvents\Schemes\Apps\.Default\WindowsLogon\.Current" "(Default)"

# Power Set standby delay to 24 hours
#powercfg /change /standby-timeout-ac 1440


### Windows Update
### --------------------------
### Windows Update
### --------------------------

if (!(Test-Path "HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate")) {New-Item -Path HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate -Type Folder | Out-Null}
# Windows Update: Don't automatically reboot after install
Set-ItemProperty "HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate" "NoAutoRebootWithLoggedOnUsers" 1d

# Windows Update: Opt-In to Microsoft Update
$MU = New-Object -ComObject Microsoft.Update.ServiceManager -Strict 
$MU.AddService2("7971f918-a847-4430-9279-4a52d1efe18d",7,"")
Remove-Variable MU

$NotepadPlusConfigFilename = "${Env:AppData}\Notepad++\config.xml"
if (test-path $NotepadPlusConfigFilename)
{
    [xml]$NotepadPlusConfig = Get-Content $NotepadPlusConfigFilename
    $RememberLastSession = $NotepadPlusConfig.NotepadPlus.GUIConfigs.GUIConfig | Where-Object  {$_.name -eq "RememberLastSession"}
    $RememberLastSession."#text" = "no"
    $NotepadPlusConfig.Save($NotepadPlusConfigFilename)
    Remove-Variable NotepadPlusConfigFilename
    Remove-Variable NotepadPlusConfig
}

# Disable 
$registryPath = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\ScPnP"
IF(!(Test-Path $registryPath))
  {
    New-Item -Path $registryPath -Force | Out-Null
}
Set-ItemProperty $registryPath "EnableScPnP" 0

$registryPath = "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\ScPnP"
if(!(Test-Path $registryPath))
{
    New-Item -Path $registryPath -Force | Out-Null
}
Set-ItemProperty $registryPath "EnableScPnP" 0

### Internet Explorer
### --------------------------

# Set home page to `about:blank` for faster loading
Set-ItemProperty "HKCU:\Software\Microsoft\Internet Explorer\Main" "Start Page" "about:blank"

# Disable Explorer file delete confirmation
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" "ConfirmFileDelete" -Value 0

# Turn off default sound alert for new Outlook email

$registryPath = "HKCU:\Software\Microsoft\Office\14.0\Outlook\Preferences"
if(Test-Path $registryPath)
{
    Set-ItemProperty $registryPath "PlaySound" -Value 0
    Set-ItemProperty $registryPath CtrlEnterSends 1
}

# Clear desktop wallpaper
Clear-ItemProperty "HKCU:\Control Panel\Desktop" "Wallpaper"

# Do not Animate windows when minimizing and maximizing
Set-ItemProperty "HKCU:\Control Panel\Desktop\WindowMetrics" "MinAnimate" "0"

# Disable Animations in Taskbar and Start Menu
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "TaskbarAnimations" 0
#Clear-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "TaskbarAnimations" 

# Disable desktop composition
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\DWM" "CompositionPolicy" 0

# Enable transparent glass
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\DWM" "ColorizationOpaqueBlend" 0

# Disable Taskbar Thumbnail Previews
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\DWM" "AlwaysHibernateThumbnails" 0

# Disable translucent selection rectangle
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "ListviewAlphaSelect" 0

# Dont show window contents while dragging
Set-ItemProperty "HKCU:\Control Panel\Desktop" "DragFullWindows" 0

# Drop shadows for icon labels on the desktop
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "ListviewShadow" 0

# Disable visual styles on windows and buttons
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\ThemeManager" "ThemeActive" "0"
$registryPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\ThemeManager"
if(Test-Path $registryPath)
{
    Clear-ItemProperty $registryPath "ThemeActive"
}

# Disable following:
# * Animate controls and elements inside windows
# * Smooth-scroll list boxes
# * Slide open combo boxes
# * Fade or slide menus into view
# * Show shadows under mouse pointer
# * Fade or slide ToolTips into view
# * Fade out menu items after clicking
# * Show shadows under windows
# * Use Visual styles on windows and buttons
Set-ItemProperty "HKCU:\Control Panel\Desktop" "UserPreferencesMask" -Value ([byte[]](0x90,0x12,0x01,0x80,0x10,0x00,0x00,0x00))

Set-ItemProperty "HKCU:\Control Panel\Bluetooth" "Notification Area Icon" "0"

$registryPath = "HKCU:\Software\Alps\Apoint\Other"
if(Test-Path $registryPath)
{
    Set-ItemProperty $registryPath "TrayIcon" "FALSE"
}

$registryPath = "HKCU:\Software\Intel\Display\igfxcui\igfxtray"
if(Test-Path $registryPath)
{
    Set-ItemProperty HKCU:\Software\Intel\Display\igfxcui\igfxtray ShowGraphicsBalloon 1
    Set-ItemProperty "HKCU:\Software\Intel\Display\igfxcui\igfxtray\TrayIcon" "ShowTrayIcon" "0"
    Set-ItemProperty "HKCU:\Software\Intel\Display\igfxcui\profiles\Display" "IsWarningEnabled" "1"
}

$registryPath = "HKCU:\Software\Valve\Steam"
if(Test-Path $registryPath)
{
    Set-ItemProperty $registryPath "StartupMode" "0"
}

$folderPath = (Join-Path $env:PUBLIC "Desktop")

[string[]] $filenamesToDelete = @(
"AnyTrans.lnk",
"iTunes.lnk",
"Mozilla Firefox.lnk",
"QuickTime Player.lnk",
"Skype.lnk",
"Steam.lnk",
"WeFi.lnk"
)

$filenamesToDelete | foreach {
    $_ = Join-Path $folderPath $_
    if (test-path $_) { Remove-Item $_ }
}

$folderPath = (Join-Path $env:USERPROFILE "Desktop")
[string[]] $filenamesToDelete = @(
"Boxstarter Shell.lnk",
"Dropbox.lnk",
"Microsoft Outlook.lnk",
"Peazip.lnk"
)

$filenamesToDelete | foreach {
    $_ = Join-Path $folderPath $_
    if (test-path $_) { Remove-Item $_ }
}

Remove-Variable folderPath
Remove-Variable filenamesToDelete

. .\googlechrome.ps1

$dotfilesWindowsExtraPath = "..\dotfiles-windows-extra\dotfiles-windows-extra.ps1"
if (test-path $dotfilesWindowsExtraPath) { . $dotfilesWindowsExtraPath }
Remove-Variable dotfilesWindowsExtraPath