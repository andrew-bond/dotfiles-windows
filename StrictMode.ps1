# Use Latest during development, then switch to latest real version tested for release
Set-StrictMode -version Latest
#Write-Host Just set Set-StrictMode

function Get-Field
{
    [CmdletBinding()]
    param (
    [Parameter(Position=0,Mandatory=$true)]
    $InputObject
    )
 
    $type = $InputObject.gettype()
 
    $publicNonPublic = [Reflection.BindingFlags]::Public -bor [Reflection.BindingFlags]::NonPublic
    $instance = $publicNonPublic -bor [Reflection.BindingFlags]::Instance
    $getField = $instance -bor [Reflection.BindingFlags]::GetField
 
    $fields = $type.GetFields($instance)
 
    $result = @{}
    $fields | Foreach-Object { $result[$_.Name] = $type.InvokeMember($_.Name, $getField, $null, $InputObject, $null) }
    $result
}
 
function Get-StrictMode
{
    ###if strict mode is set return its version, otherwise return nothing
 
    $context = (Get-Field $ExecutionContext)._context
    $sessionState = (Get-Field $context)._engineSessionState
    $scopes = Get-Field $sessionState
    $globalScope = Get-Field ($scopes._globalScope)
 
    #different hosts refer to the field differently trying to fix it
    try
    {
        $version = $globalScope."k__BackingField"
    }
    catch [System.Management.Automation.RuntimeException]
    {
        $errorId = $_.Exception.ErrorRecord.FullyQualifiedErrorId
        if (-not ($errorId -eq "PropertyNotFoundStrict"))
        {
            throw $_
        }
    }
    try
    {
        $version = $globalScope.StrictModeVersion
    }
    catch [System.Management.Automation.RuntimeException]
    {
        $errorId = $_.Exception.ErrorRecord.FullyQualifiedErrorId
        if (-not ($errorId -eq "PropertyNotFoundStrict"))
        {
            throw $_
        }
    }
    ##Major version is grater than 0 if the strict mode is set
    #test if the variable is set first to avoid strict mode error
    if ($version)
    {
        if ($Version.Major -gt 0)
        {
            $version
        }
    }
}